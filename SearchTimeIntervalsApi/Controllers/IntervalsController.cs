﻿using Microsoft.AspNetCore.Mvc;
using SearchTimeIntervalsApi.Abstractions.Services;

namespace SearchTimeIntervalsApi.Controllers
{
    [ApiController]
    [Route("/api/intervals")]
    public class IntervalsController : ControllerBase
    {
        private readonly IArrivalsService _arrivalsService;

        public IntervalsController(IArrivalsService arrivalsService)
        {
            _arrivalsService = arrivalsService;
        }

        [HttpGet("arrivals")]
        public async Task<IActionResult> GetArrivals(int offset)
        {
            try
            {
                var arrivals = await _arrivalsService.GetArrivalsAsync(offset);

                return new JsonResult(arrivals);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
