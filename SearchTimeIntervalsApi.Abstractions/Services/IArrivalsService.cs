﻿using SearchTimeIntervalsApi.Abstractions.Entites;

namespace SearchTimeIntervalsApi.Abstractions.Services
{
    public interface IArrivalsService
    {
        Task<Arrival> GetArrivalsAsync(int offset);
    }
}
