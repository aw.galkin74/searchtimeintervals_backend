﻿using Newtonsoft.Json;

namespace SearchTimeIntervalsApi.Abstractions.Entites
{
    public class Station
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("station_type")]
        public string StationType { get; set; }

        [JsonProperty("station_type_name")]
        public string StationTypeName { get; set; }
    }
}
