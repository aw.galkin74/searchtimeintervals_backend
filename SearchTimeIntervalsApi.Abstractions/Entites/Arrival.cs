﻿using Newtonsoft.Json;

namespace SearchTimeIntervalsApi.Abstractions.Entites
{
    public class Arrival
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("event")]
        public string Event { get; set; }

        [JsonProperty("station")]
        public Station Station { get; set; }

        [JsonProperty("schedule")]
        public IEnumerable<Schedule> Schedule { get; set; }

        [JsonProperty("pagination")]
        public Pagination Pagination { get; set; }
    }
}
