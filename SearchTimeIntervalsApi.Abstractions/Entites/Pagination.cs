﻿using Newtonsoft.Json;

namespace SearchTimeIntervalsApi.Abstractions.Entites
{
    public class Pagination
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("limit")]
        public int Limit { get; set; }

        [JsonProperty("offset")]
        public int Offset { get; set; }
    }
}
