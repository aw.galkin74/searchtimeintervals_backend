﻿using Newtonsoft.Json;

namespace SearchTimeIntervalsApi.Abstractions.Entites
{
    public class Schedule
    {
        [JsonProperty("arrival")]
        public DateTime Arrival { get; set; }
    }
}
