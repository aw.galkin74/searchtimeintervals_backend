﻿using Microsoft.Extensions.DependencyInjection;
using SearchTimeIntervalsApi.Abstractions.Services;
using SearchTimeIntervalsApi.Services.Arrivals;

namespace SearchTimeIntervalsApi.Services
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddArrivalsService(this IServiceCollection services)
        {
            services.AddTransient<IArrivalsService, ArrivalsService>();

            return services;
        }
    }
}
