﻿using Newtonsoft.Json;
using SearchTimeIntervalsApi.Abstractions.Entites;
using SearchTimeIntervalsApi.Abstractions.Services;
using Microsoft.Extensions.Configuration;

namespace SearchTimeIntervalsApi.Services.Arrivals
{
    public class ArrivalsService : IArrivalsService
    {
        private readonly IConfiguration _configuration;

        private readonly string uri;
        private readonly string key;

        public ArrivalsService(IConfiguration configuration)
        {
            _configuration = configuration;

            // имплементируем через конфигурацию конечный адрес yandex api
            uri = _configuration.GetSection("ApiYandexSettings")["ApiEndpointUri"];

            // имплементируем через конфигурацию ключ API
            key = _configuration.GetSection("ApiYandexSettings")["ApiKey"];
        }

        public async Task<Arrival> GetArrivalsAsync(int offset)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var dateFrom = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");

                    var result = client.GetAsync($"{uri}/schedule/?apikey={key}&station=s9600213&event=arrival&date={dateFrom}&offset={offset}").GetAwaiter().GetResult();

                    if (!result.IsSuccessStatusCode)
                    {
                        throw new HttpRequestException(message: $"Во время обработки запроса на получение данных о прилетах произошла ошибка!", new Exception(), statusCode: result.StatusCode);
                    }

                    return JsonConvert.DeserializeObject<Arrival>(await result.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
